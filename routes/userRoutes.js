
// Dependencies
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


// Routers for every task
// This is for registration
router.post("/register", userControllers.registerUser);
// This is for user login
router.post("/login", userControllers.loginUser);

// s45 Get User Details
router.get("/details", auth.verify, userControllers.getUserDetails);
router.post("/orderProduct", auth.verify, userControllers.orderProducts);

// Stretch goal
router.post("/:userId/userUpdate", userControllers.updateUserStatus);
router.get("/orders", auth.verify, userControllers.getUserOrders);
router.get("/allOrders", auth.verify, userControllers.getAllOrders);
router.delete("/deleteAll", auth.verify, userControllers.deleteAll);
router.delete("/deleteOrder/:index", auth.verify, userControllers.deleteOrder);
// Mapping
module.exports = router;