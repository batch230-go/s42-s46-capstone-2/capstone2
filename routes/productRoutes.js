
// Dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productControllers = require("../controllers/productControllers.js");


// Routers for every task
// Part s43
// Route for adding a product
router.post("/addProduct", auth.verify, productControllers.addProduct);
// Route for viewing all products (admin only)
/*
router.get("/allProducts", auth.verify, productControllers.getAllProducts);
*/
// Get all products for all access rights
router.get("/allProducts", productControllers.getAllProducts);


// Part s44
// Get or retrieve single product
router.get("/search/:productId", productControllers.getSingleProduct);
// Update a product
router.patch("/:productId/update", productControllers.updateProduct);
// Update a product status
router.patch("/:productId/archiveProduct", productControllers.updateProductStatus);


// Mapping
module.exports = router;