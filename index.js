
// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


// to create a express server/application
const app = express();


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);


// Mongoose Connection
mongoose.connect("mongodb+srv://admin:admin@batch230.pm3o66l.mongodb.net/Go-ECommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
const DB = mongoose.connection;
DB.once("open", () => console.log("Now connected to Go-MongoDB Atlas"));
DB.on("error", console.error.bind(console, "Connection error"));


// Server listen to host and port
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});



