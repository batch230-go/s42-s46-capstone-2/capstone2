
// Dependencies
const mongoose = require("mongoose");
const userControllers = require("../controllers/userControllers");


// Model Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String, 
		required: [true, "Mobile number is required"]
	},
    isAdmin: {
		type: Boolean,
		default: false
	},
	totalPurchased: {
		type: Number,
		default: 0
	},
	orders: [
		{
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			products: [
                {
                    productId: {
                        type: String,
						required: [true, "ProductId is required"]
                    },
                    productName: {
                        type: String,
						required: [true, "Product name is required"]
                    },
                    quantity : {
                        type: Number,
						default: 0
                    }
		        }
            ]
		}
	]
});
module.exports = mongoose.model("Users", userSchema);

