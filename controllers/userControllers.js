
// Dependencies
const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Products = require("../models/Products.js");

// Register User
module.exports.registerUser = (request, response) => {
	Users.findOne({$or: [{email: request.body.email}, {mobileNo: request.body.mobileNo}]})
    .then(result => {
        if(result != null && result.email == request.body.email){
            console.log("Account creation failed. Email already exist in the database");
            return response.send("Email already exist");
        }
        else if(result != null && result.mobileNo == request.body.mobileNo){
            console.log("Account creation failed. Mobile number already exist in the database");
            return response.send("Mobile number already exist");
        } 
        else{
            let newUser = new Users({
                firstName: request.body.firstName,
                lastName: request.body.lastName,
                email: request.body.email,
                password: bcrypt.hashSync(request.body.password, 10),
                mobileNo: request.body.mobileNo
            })
            newUser.save((saveError, savedUser) => {
                if(saveError){
                    console.error(saveError);
                    console.log("Account creation failed. Must fill out all necessary fields");
                    return response.send("Fill out all necessary fields");
                }
                else{
                    console.log(savedUser);
                    console.log("Account creation successful");
                    return response.status(201).send("Registration Successful");
                }
            });
	    }
	})
	.catch(error => response.send(error));
}
// User Login
module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if(result != null && result.email == request.body.email){
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            if(isPasswordCorrect){
                console.log("Login successful. Access token created");
                return response.send({access: auth.createAccessToken(result)});
            }
            else{
                console.log("Login failed. Incorrect password");
                return response.send("Incorrect password");
            }
        }
        else{
            console.log("Login failed. User not found");
            return response.send("User not found");
        }
    })
    .catch(error => response.send(error));
}


// s45 Get User Details
// Retrieve or search user details using email address
/*
module.exports.getUserDetails = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
        if(result != null && result.email == request.body.email){
            result.password = "******";
            console.log("User details found\n" + result);
            return response.send(result);
        }
        else{
            console.log("User not found");
            return response.send("User not found");
        }
	})
	.catch(error => response.send(error));
}
*/
// Retrieve user details using decoded token
module.exports.getUserDetails = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	console.log(decodedToken);
	return Users.findById(decodedToken.id)
	.then(result => {
		result.password = "******";
		response.send(result);
	})
	.catch(error => response.send(error));
}
// Order a product
module.exports.orderProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	Products.findById(request.body.productId)
	.then(async result => {
		if(result.stocks <= 0){
			console.log("Remaining stocks: " + result.stocks);
			return response.send("Product out of stock");
		}
		else if(result.stocks < request.body.quantity){
			console.log("Remaining stocks: " + result.stocks);
			return response.send("We only have " + result.stocks + " remaining stock/stocks left");
		}
		else{
			let productName = await Products.findById(request.body.productId)
			.then(result => result.name);
			let newData = {
				userId: userData.id,
				userEmail: userData.email,
				productId: request.body.productId,
				productName: productName,
				quantity: request.body.quantity
			}
			let productPrice = await Products.findById(request.body.productId)
			.then(result => result.price);
			let priceData = {
				productPrice: parseInt(productPrice)
			}
			console.log(newData);
			let isUserUpdated = await Users.findById(newData.userId)
			.then(user => {
				user.orders.push({
					totalAmount: parseInt(priceData.productPrice * request.body.quantity),
					products: {
						productId: newData.productId,
						productName: newData.productName,
						quantity: newData.quantity,
					}
				})
				user.totalPurchased = parseInt(user.totalPurchased + (priceData.productPrice * request.body.quantity));
				return user.save()
				.then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log(error);
					return false;
				})
			})
			console.log(isUserUpdated);
			let isProductUpdated = await Products.findById(newData.productId)
			.then(product => {
				product.orders.push({
					userId: newData.userId,
					userEmail: newData.userEmail,
					quantity: newData.quantity
				})
				product.stocks = product.stocks - request.body.quantity;
				return product.save()
				.then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log(error);
					return false;
				})
			})
			console.log(isProductUpdated);
			// Ternary operator
			(isUserUpdated == true && isProductUpdated == true)? 
			response.send("Order successfully placed") : response.send(false);
		}
	})
	.catch(error => response.send(error));
}


// Stretch goal
// Change user status to admin(admin access only)
module.exports.updateUserStatus = async (request, response) => {
	const hassedPassword = await auth.decode(request.headers.authorization);
	if(hassedPassword.isAdmin){
		await Users.findOneAndUpdate({_id: request.params.userId}, {isAdmin: request.body.isAdmin}, {new: true})
		.then(newUserStatus => {
			console.log(newUserStatus);
			response.send(newUserStatus);
		})
		.catch(error => {
			if(error.userId == null || error.id != request.body.userId){
				console.log("Searching failed. No User found");
				return response.send("User not found");
			}
			else
				console.log(error);
				return response.send(error);
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
// Get authenticated user's orders
module.exports.getUserOrders = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	Users.findById(decodedToken.id)
	.then(result => {
		if(result.orders.length <= 0){
			console.log("No active orders");
			response.send("No active orders");
		}
		else{
			console.log(result);
			response.send(result.orders);
		}
	})
	.catch(error => {
			console.log(error);
			response.send(error);
	})
}
// Get all orders(admin only)
module.exports.getAllOrders = async (request, response) => {
	const hassedPassword = await auth.decode(request.headers.authorization);
	if(hassedPassword.isAdmin){
		await Products.find({})
		.then(result => {
		const allOrders = result.map(product => product.orders).flat();
		console.log(result);
		response.send(allOrders);
		})
		.catch(error => {
			console.log(error);
			response.send(error);
		})
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
// Clean orders
module.exports.deleteAll = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	Users.findById(decodedToken.id)
	.then(result => {
		result.orders.splice(0, result.orders.length);
		result.totalPurchased = 0;
		result.save();
		response.send(result);
	})
	.catch(error => response.send(error));
}
// Delete specific order
module.exports.deleteOrder = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	Users.findById(decodedToken.id)
	.then(result => {
		result.orders.splice(request.params.index, 1);
		result.save();
		response.send(result);
	})
	.catch(error => response.send(error));
}

