
// Dependencies
const Products = require("../models/Products");
const auth = require("../auth.js");

// Part s43
// Creating or adding a product for admin only
module.exports.addProduct = (request, response) => {
	const hassedPassword = auth.decode(request.headers.authorization);
	let newProduct = new Products({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		stocks: request.body.stocks,
	});
	if(hassedPassword.isAdmin){
		return newProduct.save()
		.then(product => {
			console.log(product);
			response.send(product);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
// Retrieve all products for admin only
/*
module.exports.getAllProducts = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		return Products.find({}).then(result => response.send(result));
	}
	else{
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
*/
// Get all products for all access rights
module.exports.getAllProducts = (request, response) => {
	Products.find({})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}


// Part s44
// Get or retrieve single product
module.exports.getSingleProduct = async (request, response) => {
	await Products.findOne({_id: request.params.productId})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		if(error.productId == null || error.productId != request.params.productId){
			console.log("Searching failed. No product found");
			return response.send("Product not found");
		}
		else
			console.log(error);
			return response.send(error);
	});
}
// Update a product admin only
module.exports.updateProduct = async (request, response) => {
	const hassedPassword = await auth.decode(request.headers.authorization);
	if(hassedPassword.isAdmin){
		let updatedProduct = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		}
		await Products.findOneAndUpdate({_id: request.params.productId}, updatedProduct, {new: true})
		.then(newProductInfo => {
			console.log(newProductInfo);
			response.send("This item has been updated");
		})
		.catch(error => {
			console.log(error);
			response.send("Product not found");
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
// Update a product status to archive for admin only
// Achived product cannot go back to isActive: true
module.exports.updateProductStatus = async (request, response) => {
	const hassedPassword = await auth.decode(request.headers.authorization);
	if(hassedPassword.isAdmin){
		await Products.findOneAndUpdate({_id: request.params.productId}, {isActive: request.body.isActive}, {new: true})
		.then(newProductStatus => {
			console.log(newProductStatus);
			response.send(newProductStatus);
		})
		.catch(error => {			
			console.log(error);
			response.send("Product not found");
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}


